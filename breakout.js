
//ip address var
var ip_address = 'dry-reef-33369.herokuapp.com';
//canvas and context definition
var canvas = document.getElementById("myCanvas");
var ctx = canvas.getContext("2d");
ctx.canvas.width = window.innerWidth;
ctx.canvas.height = window.innerHeight;
//thumb detection variable
var detectDiv = document.getElementById("myDetectDiv");
var txtScore = document.getElementById("txtScore");
var txtLives = document.getElementById("txtLives");
//ball variables
var x = canvas.width / 2;
var y = canvas.height - 100;
var dx = 10;
var dy = -10;
var ballRadius = 10;
//paddle variables
var paddleWidth = 180;
var paddleHeight = 30;
var paddleX = (canvas.width - paddleWidth) / 2;
//event variables
var rightPressed = false;
var leftPressed = false;
//player variables
var score = 0;
var lives = 3;
//brick variables
var brickRowCount = 4;
var brickColCount = 9;
var brickWidth = 100;
var brickHeight = 50;
var brickPadding = 2;
var brickOffsetTop = 20;
var brickOffsetLeft = 25;

//Breakout logic
var bricks = [];
for (c = 0; c < brickColCount; c++) {
    bricks[c] = [];
    for (var r = 0; r < brickRowCount; r++) {
        bricks[c][r] = { x: 0, y: 0, status: 1 };
    }
}
function drawBricks() {
    for (c = 0; c < brickColCount; c++) {
        for (r = 0; r < brickRowCount; r++) {
            if (bricks[c][r].status == 1) {
                var brickX = (c * (brickWidth + brickPadding)) + brickOffsetLeft;
                var brickY = (r * (brickHeight + brickPadding)) + brickOffsetTop;
                bricks[c][r].x = brickX;
                bricks[c][r].y = brickY;

                var image = new Image();
                switch (r) {

                    case 0: image.src = 'https://'+ip_address+'//Puzzle%20Pack%20II//PNG//Tiles yellow//tileYellow_38.png';
                        ctx.drawImage(image, brickX, brickY, brickWidth, brickHeight);
                        break;
                    case 1: image.src = 'https://'+ip_address+'//Puzzle%20Pack%20II//PNG//Tiles blue//tileBlue_38.png';
                        ctx.drawImage(image, brickX, brickY, brickWidth, brickHeight);
                        break;
                    case 2: image.src = 'https://'+ip_address+'//Puzzle%20Pack%20II//PNG//Tiles pink//tilePink_38.png';
                        ctx.drawImage(image, brickX, brickY, brickWidth, brickHeight);
                        break;
                    case 3: image.src = 'https://'+ip_address+'//Puzzle%20Pack%20II//PNG//Tiles red//tileRed_38.png';
                        ctx.drawImage(image, brickX, brickY, brickWidth, brickHeight);
                        break;

                    default: image.src = 'https://'+ip_address+'//Puzzle%20Pack%20II//PNG//Tiles red//tileRed_38.png';
                        ctx.drawImage(image, brickX, brickY, brickWidth, brickHeight);
                        break;

                }
            }
        }
    }
}

function drawPaddle() {
    var image = new Image();
    image.src = 'https://'+ip_address+'//Puzzle%20Pack%20II//PNG//Paddles//paddle_01.png';
    ctx.drawImage(image, paddleX, canvas.height - paddleHeight, paddleWidth, paddleHeight);
}

function drawBall() {
    var image = new Image();
    image.src = 'https://'+ip_address+'//Puzzle%20Pack%20II//PNG//Balls//Yellow//ballYellow_10.png'
    ctx.drawImage(image, x, y, ballRadius * 2, ballRadius * 2);

}

function draw() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    drawBricks();
    drawBall();
    drawPaddle();
    drawScore();
    drawLives();
    collisionDetection();
    x += dx;
    y += dy;

    if (y + dy < ballRadius)
        dy = -dy;
    else if (y + dy > canvas.height - paddleHeight - dy) {
        if (x > paddleX && x < paddleX + paddleWidth) {
            dy = -dy;
        }
        else {
            //alert("Game Over bitch!");
            lives--;
            if (lives <= 0)
                document.location.reload();
            else {
                x = canvas.width / 2;
                y = canvas.height - 100;
                dx = 10;
                dy = -10;
                paddleX = (canvas.width - paddleWidth) / 2;
            }
        }
    }
    if (x + dx > canvas.width - ballRadius || x + dx < ballRadius)
        dx = -dx;
   
    requestAnimationFrame(draw);
}
document.addEventListener("keydown", keydownHandler, false);
document.addEventListener("keyup", keyupHandler, false);
document.addEventListener("mousemove", mouseMoveHandler, false);

if (detectDiv) {
    detectDiv.addEventListener('touchmove', function (e) {
        var relativeX = e.touches[0].clientX - canvas.offsetLeft;
        if (relativeX > 0 && relativeX < canvas.width)
            paddleX = relativeX - paddleWidth / 2;
        e.preventDefault();
    }, false);
}
function keydownHandler(e) {
    if (e.keyCode == 39)
        rightPressed = true;
    else if (e.keyCode == 37)
        leftPressed = true;
}
function keyupHandler(e) {
    if (e.keyCode == 39)
        rightPressed = false;
    else if (e.keyCode == 37)
        leftPressed = false;
}
function mouseMoveHandler(e) {
    var relativeX = e.clientX - canvas.offsetLeft;
    if (relativeX > 0 && relativeX < canvas.width)
        paddleX = relativeX - paddleWidth / 2;
    e.preventDefault();
}

function collisionDetection() {
    for (c = 0; c < brickColCount; c++) {
        for (r = 0; r < brickRowCount; r++) {
            var b = bricks[c][r];
            if (b.status == 1) {
                if (x > b.x && x < b.x + brickWidth && y > b.y && y < b.y + brickHeight) {
                    dy = -dy;
                    b.status = 0;
                    score++;
                    if (score == brickColCount * brickRowCount) {
                        alert("CONGRATULATIONS!! YOU WIN!");
                        document.location.reload();
                    }
                }
            }
        }
    }
}
function drawScore() {
    txtScore.innerText = score;
}

function drawLives() {
    txtLives.innerText = lives;
}
draw();